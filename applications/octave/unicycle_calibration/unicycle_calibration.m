global polynomial_coefficients= ...
[ 1  0     -1/6   0     1/120  0;
  0  1/2    0    -1/24  0      1/720
];

function e=error(x,z,ticks)
  tr=ticks(1);
  tl=ticks(2);
  kr=x(1);
  kl=x(2);
  b=x(3);
  T=[tr, tl; tr, -tl];
  delta=T*[kr; kl];
  theta=delta(1)/b;
  x=delta(2)/2*polyval(p(1,:),theta);
  y=delta(2)/2*polyval(p(2,:),theta);
  e=[x; y; theta] - z;
endfunction

function J=numericJacobian(x,z,ticks)
  J=zeros(3,3);
  epsilon=1e-6;
  delta_epsilon=zeros(3);
  iepsilon=2/delta_epsilon;
  for (i=1:3)
    delta_epsilon(i)=epsilon;
    J(:,i)=error(x+delta_epsilon,z,ticks)-error(x-delta_epsilon,z,ticks);
    delta_epsilon(i)=0;
  endfor;
  J*=iepsilon;
endfunction;

function [e,J]=errorAndJacobian(x,z,ticks)
  e=error(x,z,ticks)l
  J=numericJacobian(x,z,ticks);
endfunction;

global min_ticks=1e-3;

function [X, chi] = oneIteration(X, Z)
  b=zeros(3,1);
  H=zeros(3,3);
  chi=0;
  for (i=1:size(Z,1))
    ticks=Z(i,1:2);
    # skip small measurements
    if (ticks(1)<min_ticks &&
        ticks(2)<min_ticks)
      continue;
    endif;
    z=Z(i,3:5);
    [e,J]=errorAndJacobian(X,z,ticks);
    b+=J'*e;
    H+=J'*J;
    chi+=e'*e;
  endfor;
  dx=-H\b;
  x+=dx;
endfunction;
